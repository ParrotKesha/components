<?

use Bitrix\Main\Data\Cache;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CatalogItemComponent extends CBitrixComponent
{
    public function onPrepareComponentParams($params)
    {
        return $params;
    }

    public function executeComponent()
    {
        $this->arResult['elements'] = $this->getData();
        $this->includeComponentTemplate();
    }
    private function getData()
    {
        $obCache = Cache::createInstance();
        if ($obCache->initCache(60, serialize(['123']), '/city_get')) {
            $result = $obCache->getVars();
        }
        elseif ($obCache->startDataCache()) {
            CModule::IncludeModule("iblock");

            $arFilter = Array("IBLOCK_ID"=>4, "IBLOCK_SECTION_ID"=>16, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
            $arSelect = Array("ID", "IBLOCK_ID", "FIRSTNAME", "LASTNAME");

            $res = CIBlockElement::getList(Array(), $arFilter, false, false, $arSelect);
            $result = array();
            while($ob = $res->GetNextElement()) {
                $arProps = $ob->GetProperties();
                $str = $arProps["LASTNAME"]["VALUE"]." ".$arProps["FIRSTNAME"]["VALUE"];
                array_push($result, $str);
            }
            $obCache->endDataCache($result);
        }

        return $result;
    }
}
